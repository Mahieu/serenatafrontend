import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Product } from '../models/product.model';
import { ProductService } from '../product.service';
import { UserProductService } from '../userproduct.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  public product : Product = new Product();
  public isLoggedIn : boolean = false;
  constructor(private productService : ProductService,private route: ActivatedRoute, private userProductService : UserProductService, private authService : AuthService, private snackbar : MatSnackBar) { }
  ngOnInit(): void {
    this.authService.isLoggedIn$.subscribe((res) => {
      this.isLoggedIn = res;
    })
  }
  ngAfterViewInit(): void
  {
    const productId = Number(this.route.snapshot.paramMap.get('id'));
    this.productService.getById(productId).subscribe((res) => {
      this.product = res;
    });
  }
  addItemToCart() : void {
    this.userProductService.addProductToUser(
      this.product
    );
    this.snackbar.open("🛒 "+this.product.name+" added to the cart 🛒");

  }
}
