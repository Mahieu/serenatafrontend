import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from './models/product.model';
import { UserProduct } from './models/userProduct.model';
const API_URL = environment.APIURL+"userproducts";
@Injectable({
  providedIn: 'root'
})
export class UserProductService {
  private userProducts = new BehaviorSubject<UserProduct[]>([]);
  userProducts$ = this.userProducts.asObservable();
  constructor(private httpClient : HttpClient) {
    this.loadUserProducts();
  }
  loadUserProducts() : void 
  {
    this.getAll().subscribe((res) => {
      this.setUserProducts(res);
    });
  }
  setUserProducts(userProducts : UserProduct[]){
    this.userProducts.next(userProducts);
  }
  addProductToUser(product : Product)
  {
    let tmpProductList : UserProduct[] = this.userProducts.getValue();
    let foundedUserProduct = tmpProductList.find(up => up.productId == product.id);
    if(foundedUserProduct == undefined)
    {
      const newUserProduct : UserProduct = {id :0,product : product, user : null,userId : localStorage.getItem("userId"),productId : product.id,qty : 1};
      this.create(newUserProduct).subscribe((res : UserProduct) => {
        tmpProductList.push(res);
        this.setUserProducts(tmpProductList);
      });
    }
    else{
      foundedUserProduct.qty++;
      this.update(foundedUserProduct).subscribe((res) => {
        this.setUserProducts(tmpProductList);
      });
    }
  }
  getAll() : Observable<UserProduct[]>
  {
    return this.httpClient.get<UserProduct[]>(API_URL);
  }
  create(userProduct: UserProduct) : Observable<UserProduct>{
    return this.httpClient.post<UserProduct>(API_URL,userProduct);
  }
  update(userProduct: UserProduct) : Observable<UserProduct>
  {
    return this.httpClient.put<UserProduct>(API_URL+"/"+userProduct.id,userProduct);
  }
  delete(id : number)
  {
    return this.httpClient.delete<UserProduct>(API_URL+"/"+id);
  }
  deleteAll()
  {
    let tmpProductList : UserProduct[] = this.userProducts.getValue();
    tmpProductList.forEach(up => {
      this.delete(up.id).subscribe((res) => {});
    });
    this.setUserProducts([]);
  }

}