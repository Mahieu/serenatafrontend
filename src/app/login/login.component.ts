import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { LoginResult } from '../models/login-result.model';
import { User } from '../models/user.model';
import { UserProductService } from '../userproduct.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email : new FormControl('',[Validators.required, Validators.email]),
    password : new FormControl('',[Validators.pattern('^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=\\D*\\d)[A-Za-z\\d!$%@#£€*?&]{8,}$'), Validators.required]),  
  });

  constructor(private authService : AuthService,private snackbar : MatSnackBar, private router : Router,private userProductService : UserProductService) { }

  ngOnInit(): void {
  }
  onSubmit() {
    let user : User = new User();
    user.email = this.email.value;
    user.password = this.password.value;
    this.authService.login(user).subscribe((res : LoginResult) => {
      localStorage.setItem("token",res.token);
      localStorage.setItem("expiration", new Date(res.expirationDate).toISOString());
      localStorage.setItem("userId",res.userId);
      this.authService.setIsLoggedIn(true);
      this.authService.setRoles(res.roles);
      this.userProductService.loadUserProducts();
      this.router.navigateByUrl('/');
      this.snackbar.open("✔️ Login succeeded ✔️");
    },(err) => {
      this.snackbar.open("❌ The email/password is not correct ❌");
    });

  }
  get email() { return this.loginForm.get('email')}
  get password() { return this.loginForm.get('password')}
}
