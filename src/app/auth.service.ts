import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginResult } from './models/login-result.model';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API_URL : string = environment.APIURL + 'auth/';
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this.isLoggedIn.asObservable();
  constructor(private httpClient : HttpClient ) {
    this.setIsLoggedIn(this.checkExpiration());
  }
  login(user : User) : Observable<LoginResult>{
    return this.httpClient.post<LoginResult>(this.API_URL+'login',user);
  }
  register(user : User) : Observable<any>
  {
    return this.httpClient.post<any>(this.API_URL+'register',user);
  }
  checkExpiration() : boolean
  {
    if(Number.isNaN(Date.parse(localStorage.getItem("expiration")))){
      return false;
    }
    return Date.now() <= Date.parse(localStorage.getItem("expiration"));
  }
  refreshToken(){

  }
  setIsLoggedIn(val : boolean){
    this.isLoggedIn.next(val);
  }
  getRole() : string
  {
    return localStorage.getItem("role");
  }
  setRoles(roles : string[])
  {
    localStorage.setItem("role",JSON.stringify(roles));
  }
  logout() : void{
    localStorage.clear();
    this.setIsLoggedIn(false);
  }

}
