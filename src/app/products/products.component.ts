import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public filteredProducts : Product[];
  public products : Product[];
  public timer : any;
  public search : string = "";
  constructor(private productService : ProductService) { }

  ngOnInit(): void {
    this.productService.filteredProducts$.subscribe((res) => {
      this.filteredProducts = res;
    });
    this.productService.products$.subscribe((res) => {
      this.products = res;
    });
  }
  filter()
  {
    this.timer = setTimeout(() => {
      this.productService.setFilteredProducts(
        this.products.filter((p) => p.name.toLocaleLowerCase().includes(this.search))
      );
    }, 750);

  }
  reset()
  {
    this.search = "";
    this.productService.setFilteredProducts(this.products);
  }
}
