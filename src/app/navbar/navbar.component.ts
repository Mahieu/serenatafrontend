import {
  Component,
  OnInit
} from '@angular/core';
import {
  AuthService
} from '../auth.service';
import { UserProductService } from '../userproduct.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean = false;
  nbItems : number = 0;
  constructor(private authService: AuthService, private userProductService : UserProductService) {
    this.authService.isLoggedIn$.subscribe((res) => {
      this.isLoggedIn = res;
      this.userProductService.userProducts$.subscribe((res) => {
        this.nbItems = res.length;
      })
    });
  }
  disconnect() : void {
    this.authService.logout();
  }
  ngOnInit(): void {}

}
