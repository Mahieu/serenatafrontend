import { Component, OnInit, ViewChild } from '@angular/core';
import { UserProduct } from '../models/userProduct.model';
import { MatTable } from '@angular/material/table';
import { UserProductService } from '../userproduct.service';
@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {
  userProducts : UserProduct[];
  displayedColumns : string[] = ["name","quantity","price","actions"];
  total : number = 0
  @ViewChild(MatTable) table: MatTable<UserProduct>;
  constructor(private userProductService : UserProductService) { 
    this.userProductService.userProducts$.subscribe((res) => {
      this.userProducts = res;
      this.calculateTotal();
    });
  }
  ngOnInit(): void {
  }
  clearBasket() : void
  {
    this.userProductService.deleteAll();
    this.calculateTotal();
  }
  addQty(up : UserProduct)
  {
    up.qty++;
    this.userProductService.update(up).subscribe((res) => {});
    this.calculateTotal();
  }
  removeQty(up : UserProduct)
  {
    up.qty--;
    this.userProductService.update(up).subscribe((res) => {});
    this.calculateTotal();
  }
  delete(up : UserProduct)
  {
    this.userProductService.delete(up.id).subscribe((res) => {});
    this.userProductService.setUserProducts(this.userProducts.filter(currentElement => currentElement.id !== up.id ))
    this.calculateTotal();
  }
  private calculateTotal()
  {
    this.total = 0;
    //I could use reduce but I need time
    this.userProducts.forEach((up)=> {
      this.total+= up.qty * up.product.price;
    });
  }
  
}
