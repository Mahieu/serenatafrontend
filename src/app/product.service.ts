import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from './models/product.model';
const API_URL = environment.APIURL+"products";
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private products = new BehaviorSubject<Product[]>([]);
  products$ = this.products.asObservable();
  private filteredProducts = new BehaviorSubject<Product[]>([]);
  filteredProducts$ = this.filteredProducts.asObservable();
  constructor(private httpClient : HttpClient) {
    this.loadProducts();
  }
  loadProducts() : void{
    this.getAll().subscribe((res) => {
      this.setProducts(res);
      this.setFilteredProducts(res);
    });
  }
  setProducts(products : Product[]){
    this.products.next(products);
  }
  setFilteredProducts(products : Product[])
  {
    this.filteredProducts.next(products);
  }
  getAll() : Observable<Product[]>
  {
    return this.httpClient.get<Product[]>(API_URL);
  }
  getById(id : number) : Observable<Product>{
    return this.httpClient.get<Product>(API_URL+"/"+id);
  }
}
