import { UserProduct } from './userProduct.model';

export class User {
    id : string;
    email : string;
    password : string;
    username : string;
    userProducts : UserProduct[];
}
