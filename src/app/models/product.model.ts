import { UserProduct } from './userProduct.model';

export class Product{
    id:number;
    name : string;
    description : string;
    pictureURL : string;
    qty : number;
    price : number;
    userProducts : UserProduct[];

}