import { Product } from './product.model';
import { User } from './user.model';

export class UserProduct {
    id : number;
    userId : string;
    user : User;
    productId: number;
    product: Product;
    qty : number;
}