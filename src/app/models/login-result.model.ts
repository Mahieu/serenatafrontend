export class LoginResult {
    username : string;
    roles : string[];
    token : string;
    expirationDate: Date;
    userId : string;
}