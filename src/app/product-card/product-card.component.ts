import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../models/product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  @Input() product: Product;
  public color : string = "rgba(224,255,255,0.5)";
  constructor(private productService : ProductService, private router : Router) { }

  ngOnInit(): void {
  }
  selectProduct()
  {
    this.router.navigateByUrl('/products/'+this.product.id);
  }
}
